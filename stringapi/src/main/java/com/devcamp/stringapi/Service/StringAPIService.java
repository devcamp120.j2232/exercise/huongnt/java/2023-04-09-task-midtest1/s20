package com.devcamp.stringapi.Service;

import java.util.Arrays;

import org.springframework.stereotype.Service;

@Service
public class StringAPIService {

    // 1. reverse input String
    public String reverseStringInJava(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = input.length() - 1; i >= 0; i--) {
            sb.append(input.charAt(i));
        }
        return sb.toString();
    }

    // 2. check palindrome string
    public boolean isPalindrome(String input) {
        if (input == null) {
            return false;
        }
        StringBuilder stringBuilder = new StringBuilder(input);
        return input.equals(stringBuilder.reverse().toString());
    }

    // 3. remove Duplicate character in String
    public String removeDuplicateString(String input) {
        char str3[] = input.toCharArray();
        int n1 = str3.length;
        int index = 0;
        for (int i = 0; i < n1; i++) {

            // Check if str[i] is present before it
            int j;
            for (j = 0; j < i; j++) {
                if (str3[i] == str3[j]) {
                    break;
                }
            }

            // If not present, then add it to
            // result.
            if (j == i) {
                str3[index++] = str3[i];
            }
        }
        return String.valueOf(Arrays.copyOf(str3, index));
    }

    //4 concatenate and cut String
    public String concatenateStrings(String s1, String s2) {
        if (s1.length() > s2.length()) {
            s1 = s1.substring(s1.length() - s2.length());
        } else if (s2.length() > s1.length()) {
            s2 = s2.substring(s2.length() - s1.length());
        }
        return s1 + s2;
    }
}
