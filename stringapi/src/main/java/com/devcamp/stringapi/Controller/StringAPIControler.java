package com.devcamp.stringapi.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.stringapi.Service.StringAPIService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class StringAPIControler {
    @Autowired
    public StringAPIService stringAPIService;

    //1. api get reverse input String
    @GetMapping("/reverse")
    public String getReverseString(@RequestParam(name = "string", required = true) String inputString){
        return stringAPIService.reverseStringInJava(inputString);
    }

    //2. api check palindrome string
    @GetMapping("/palindrome")
    public boolean checkPalindrome(@RequestParam(name = "string", required = true) String inputString){
        return stringAPIService.isPalindrome(inputString);
    }

    //3 api remove Duplicate character in String
    @GetMapping("/removeDup")
    public String removeDulString(@RequestParam(name = "string", required = true) String inputString){
        return stringAPIService.removeDuplicateString(inputString);
    }

    //
    @GetMapping("/cut-dup")
    public String cutAndDulString(@RequestParam String str1, String str2){
        return stringAPIService.concatenateStrings(str1,str2);
    }
  
}
